import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {

        HashMap<String, Integer> gamesInventory = new HashMap<>();

        gamesInventory.put("Mario Odyssey", 50);
        gamesInventory.put("Super Smash Bros. Ultimate", 20);
        gamesInventory.put("Luigi's Mansion 3", 15);
        gamesInventory.put("Pokemon Sword", 30);
        gamesInventory.put("Pokemon Shield", 100);

        gamesInventory.forEach((game, inventory) -> {
            System.out.println(game + " has " + inventory + " stocks left.");
        });

        ArrayList<String> topGames = new ArrayList<>();

        gamesInventory.forEach((game, inventory) -> {
            if(inventory<=30){
                topGames.add(game);
                System.out.println(game + " has been added to top games list!");
            }
        });

        System.out.println("Our shop's top games: " + topGames);




    }
}